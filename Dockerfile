FROM ruby:2.7

VOLUME /usr/src/gems

ENV GEM_HOME /usr/src/gems
ENV PATH $PATH:$GEM_HOME/bin:$GEM_HOME/gems/bin
ENV RACK_ENV=development

VOLUME /usr/src/app
WORKDIR /usr/src/app
